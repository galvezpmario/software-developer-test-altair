package softwareDeveloperTestAltair.arraySort;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import softwareDeveloperTestAltair.arraySort.polygon.Circle;
import softwareDeveloperTestAltair.arraySort.polygon.Rectangle;
import softwareDeveloperTestAltair.arraySort.polygon.Triangle;
import softwareDeveloperTestAltair.arraySort.sortingCriteria.SortingCriteriaEnum;

public class ArraySorterTests {
	
	@Test
	public void testAscendingSorting() {
		Circle circle = new Circle("circle-1", 3);
		Triangle triangle = new Triangle("triangle-1", 3, 5);
		Rectangle rectangle = new Rectangle("rectangle-1", 3, 5);
		Object[] polygonArray = {circle, triangle, rectangle};
		ArraySorter sorter = new ArraySorter(polygonArray) ;
		
		Object[] sortedPolygons = sorter.sort(SortingCriteriaEnum.ASCENDING);
		
		assertEquals(sortedPolygons[0], triangle);
		assertEquals(sortedPolygons[1], rectangle);
		assertEquals(sortedPolygons[2], circle);
	}
	
	//... Whatever you want to add.
}
