package softwareDeveloperTestAltair.arraySort;

import java.util.stream.Stream;

import softwareDeveloperTestAltair.arraySort.sortingCriteria.SortingCriteriaEnum;

public class ArraySorter {

	
	
	private Object[] polygons = {};
	
	public ArraySorter(Object[] polygons) {
		this.polygons = polygons;
	}
	
	// TODO: Implement
	public Object[] sort(SortingCriteriaEnum criteria) {
		return null;
	}
	
	public void printSortedPolygons(Object[] sortedPolygons) {
		Stream.of(sortedPolygons).forEachOrdered(x->System.out.println(x.toString()));
	}
	
	public static void main(String...args) {
		// Here you can play with your implementation, but please don't forget tests.
	}
}
