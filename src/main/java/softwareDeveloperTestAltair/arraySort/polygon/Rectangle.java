package softwareDeveloperTestAltair.arraySort.polygon;

public class Rectangle {
	
	private float base;
	private float heigth;
	private String name;
	
	public Rectangle(String name, float base, float height) {
		this.name = name;
		this.base = base;
		this.heigth = height;
	}
	
	public float getArea() {
		return base * heigth;
	}
	
	// Implement
	@Override
	public String toString() {
		return null;
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getHeigth() {
		return heigth;
	}

	public void setHeigth(float heigth) {
		this.heigth = heigth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
}
