package softwareDeveloperTestAltair.arraySort.polygon;

/**
 * For the shake of simplicity, we'll consider all triangles to be rectangles :)
 */
public class Triangle {
	private float base;
	private float heigth;
	private String name;
	
	public Triangle(String name, float base, float height) {
		this.name = name;
		this.base = base;
		this.heigth = height;
	}
	
	public float getArea() {
		return base * heigth / 2;
	}
	
	// Implement
	@Override
	public String toString() {
		return null;
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public float getHeigth() {
		return heigth;
	}

	public void setHeigth(float heigth) {
		this.heigth = heigth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
