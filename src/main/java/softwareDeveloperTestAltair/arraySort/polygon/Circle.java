package softwareDeveloperTestAltair.arraySort.polygon;

public class Circle {

	private static double PI = Math.PI;
	private float radius;
	private String name;
	
	public Circle(String name, float radius) {
		this.radius = radius;
		this.name = name;
	}
	
	public float getArea() {
		return (float) (PI * Math.pow(radius, 2));
	}
	
	// Implement
	@Override
	public String toString() {
		return null;
	}
	 
	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
